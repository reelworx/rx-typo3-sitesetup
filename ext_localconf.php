<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
            '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extKey . '/Configuration/TsConfig/User/">'
        );

        /** @var \TYPO3\CMS\Core\Configuration\ConfigurationManager $configManager */
        $configManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ConfigurationManager::class);

        $originalConfig = $sysConfig = $configManager->getLocalConfiguration();

        $applicationContext = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();

        // even though this should actually always be true there might be edge cases where this is empty
        if (is_array($sysConfig)) {
            $sysConfig['BE']['debug'] = false;
            $sysConfig['BE']['lockIP'] = 0;
            $sysConfig['BE']['sessionTimeout'] = 3600;
            $sysConfig['BE']['compressionLevel'] = 5;
            $sysConfig['BE']['versionNumberInFilename'] = true;
            $sysConfig['BE']['loginSecurityLevel'] = 'normal';
            $sysConfig['FE']['debug'] = false;
            $sysConfig['FE']['compressionLevel'] = 5;
            // note: it might be necessary to write this to $GLOBALS if some other extension is setting this during runtime as well
            $sysConfig['FE']['pageNotFound_handling'] = 'USER_FUNCTION:' . Reelworx\Sitesetup\Hooks\PageNotFoundHandler::class . '->handle';
            $sysConfig['FE']['versionNumberInFilename'] = 'embed';
            $sysConfig['FE']['loginSecurityLevel'] = 'normal';
            $sysConfig['FE']['hidePagesIfNotTranslatedByDefault'] = true;
            $sysConfig['SYS']['sitename'] = 'sitesetup';
            $sysConfig['SYS']['displayErrors'] = 0;
            $sysConfig['SYS']['ddmmyy'] = 'd.m.Y';
            $sysConfig['SYS']['UTF8filesystem'] = true;
            $sysConfig['SYS']['systemLocale'] = 'en_US.utf8';
            $sysConfig['SYS']['exceptionalErrors'] = 4096;
            $sysConfig['MAIL']['transport'] = 'smtp';
            $sysConfig['MAIL']['transport_smtp_server'] = 'localhost:25';
            $sysConfig['MAIL']['defaultMailFromAddress'] = 'support@reelworx.at';
            $sysConfig['MAIL']['defaultMailFromName'] = 'support@reelworx.at';

            if ($applicationContext->isDevelopment()) {
                $sysConfig['BE']['debug'] = true;
                $sysConfig['SYS']['sitename'] = $sysConfig['SYS']['sitename'] . ' (Development)';
                $sysConfig['SYS']['exceptionalErrors'] = 12290;
                $sysConfig['SYS']['displayErrors'] = 1;

                $sysConfig['MAIL']['defaultMailFromAddress'] = 'support@reelworx.at';
                $sysConfig['MAIL']['transport_smtp_server'] = 'dev.reelworx.at:1025';
                //$sysConfig['MAIL']['transport'] = 'mbox';
                //$sysConfig['MAIL']['transport_mbox_file'] = PATH_site . 'mails.txt';
            }

            if ($sysConfig !== $originalConfig) {
                $configManager->writeLocalConfiguration($sysConfig);
            }
        }

        $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['default_editor_preset'] = 'EXT:' . $extKey . '/Configuration/RTE/Default.yaml';

        $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class);

        // configure scheduler
        $schedulerConfig = $extensionConfiguration->get('scheduler');
        if ($schedulerConfig['showSampleTasks']) {
            $extensionConfiguration->set('scheduler', 'showSampleTasks', 0);
            $extensionConfiguration->set('scheduler', 'enableBELog', 0);
        }

        // style BE login
        $logo = 'EXT:' . $extKey . '/Resources/Public/Images/logo.png';
        $beImage = 'EXT:' . $extKey . '/Resources/Public/Images/belogin_background.jpg';

        $backendConfig = $extensionConfiguration->get('backend');
        if ($backendConfig['loginBackgroundImage'] !== $beImage) {
            $extensionConfiguration->set('backend', 'loginLogo', $logo);
            $extensionConfiguration->set('backend', 'loginBackgroundImage', $beImage);
        }

        $GLOBALS['TYPO3_CONF_VARS']['LOG']['Reelworx']['RxShariff']['writerConfiguration'] = [
            \TYPO3\CMS\Core\Log\LogLevel::WARNING => [
                \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
                    'logFileInfix' => 'shariff'
                ]
            ]
        ];
        $GLOBALS['TYPO3_CONF_VARS']['LOG']['TYPO3']['CMS']['Frontend']['processorConfiguration'] = [
            \TYPO3\CMS\Core\Log\LogLevel::ERROR => [
                \TYPO3\CMS\Core\Log\Processor\WebProcessor::class => []
            ]
        ];
    },
    $_EXTKEY
);
