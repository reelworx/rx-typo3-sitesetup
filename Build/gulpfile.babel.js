import gulp     from 'gulp';
import plugins  from 'gulp-load-plugins';

const $ = plugins();

const Tasks = {
  devContext: true,
  sources: {
    css: '../Resources/Public/Scss/*.scss',
    js: [
      'node_modules/jquery/dist/jquery.js',
      'node_modules/popper.js/dist/umd/popper.js',
      'node_modules/bootstrap/js/dist/util.js',
      'node_modules/bootstrap/js/dist/carousel.js',
      'node_modules/bootstrap/js/dist/modal.js',
      'node_modules/bootstrap/js/dist/dropdown.js',
      'node_modules/bootstrap/js/dist/collapse.js',
      //'node_modules/ekko-lightbox/dist/ekko-lightbox.js',
      '../Resources/Public/JavaScript/src/lib/*.js',
      '../Resources/Public/JavaScript/src/*.js'
    ]
  },

  setProduction: function (done) {
    Tasks.devContext = false;
    return done();
  },
  sass: function () {
    let cssPath = '../Resources/Public/Css/';
    let scssPaths = [
      'node_modules/'
    ];

    return gulp
      .src(Tasks.sources.css)
      .pipe($.sass({
        outputStyle: Tasks.devContext ? '' : 'compressed',
        includePaths: scssPaths
      }).on('error', $.sass.logError))
      .pipe($.autoprefixer({
        cascade: false
      }))
      .pipe(gulp.dest(cssPath));
  },
  js: function () {
    let src = gulp.src(Tasks.sources.js);
    if (!Tasks.devContext) {
      src = src.pipe($.uglify());
    }
    return src
      .pipe($.concat('main.js'))
      .pipe(gulp.dest('../Resources/Public/JavaScript/'));
  },
  icons: function() {
    return gulp
      .src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
      .pipe(gulp.dest('../Resources/Public/fonts/'));
  },
  watch: function (done) {
    gulp.watch(Tasks.sources.css, gulp.series('sass'));
    gulp.watch(Tasks.sources.js, gulp.series('js'));
    return done();
  }
};

gulp.task('sass', Tasks.sass);
gulp.task('js', Tasks.js);
gulp.task('icons', Tasks.icons);
gulp.task('watch', Tasks.watch);

gulp.task('setProduction', Tasks.setProduction);

// combined tasks
gulp.task('release', gulp.series('setProduction', gulp.parallel('sass', 'js', 'icons')));
gulp.task('dev',gulp.parallel('sass', 'js', 'icons'));

gulp.task('default', function(done) {
  process.stdout.write('\n'
    + '===========================================\n'
    + 'Tasks: dev, release, watch\n'
    + '===========================================\n'
    + '\n'
  );
  return done();
});

// ################### REMOVAL_POINT ######################
// Everything below here is removed after the create-task has been run.

// -------------------------
const extKey = 'complex_extension-key'; // extension key (extension folder name)
const extName = 'ComplexExtension-key'; // extbase extension name
// -------------------------

gulp.task('default', function(done) {
  if (extKey === 'complex_extension-key') {
    process.stdout.write('\n'
      + '===========================================\n'
      + 'Please provide the extension key and name in the gulpfile.babel.js file\n'
      + 'to automigrate this site setup for the concrete project.\n'
      + '===========================================\n'
      + '\n'
    );
  } else {
    gulp.src('../**/*.{xlf,php,ts,typoscript,tsconfig,html,json,yaml}')
      .pipe($.replace('sitesetup', extKey, {skipBinary: true}))
      .pipe($.replace('Sitesetup', extName, {skipBinary: true}))
      .pipe(gulp.dest('../'));
    gulp.src('gulpfile.babel.js').pipe($.replace(/\n\/\/ #+ REMOVAL_POINT(:?.|\n)*/m, '')).pipe(gulp.dest('.'));
    gulp.src('package.json').pipe($.replace(/^.*gulp-replace[^\n]+\n/m, '')).pipe(gulp.dest('.'));
  }
  return done();
});
