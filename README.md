TYPO3 CMS site setup extension
==============================

This is a template site setup extension (theming, bootstrap, sitepackage, you name it) for TYPO3 CMS 9. 
This extension provides all tools to set up a complete website.

The HTML and CSS structure is based on the bootstrap (http://getbootstrap.com) framework version 4,
but is easily replaceable with any other framework of your choice.
Take a look into the `Build` folder to adjust the dependencies. 

The contained TypoScript samples comprise the base setup for fluid_styled_content.

This extension is updated on a regular basis to reflect the current developments of TYPO3.

How to build things
-------------------

We use `gulp` to generate CSS and javascript.

* Install [node.js](https://nodejs.org) on your machine
* Install [yarn](https://yarnpkg.com) on your machine
* `git clone --depth 1 -b master https://bitbucket.org/reelworx/rx-typo3-sitesetup.git <your_ext_key>`
* `cd <your_ext_key>/Build`
* Run `yarn` to install all dependencies
* Run `yarn gulp` for further instructions

Page-not-found handling
-----------------------

The extension ships a pageNotFound-handler that redirects the user to a page
with the id `routing` if an access violation has occurred.
Otherwise the behaviour is like `[FE][pageNotFound_handling] = true`

New website check list
----------------------

- robots.txt reachable and correct
- access protection (by default .htaccess) working
- Google Webmaster Tools registration
- href-lang tags correct
- canonical tag set
- sitemap.xml contains only valid urls
- page title and metatags for detail pages (like news) correct
- structured data sent and correct
- Google Page Speed checked
- favicon installed
- TYPO3 context set to production
- cronjob setup (`0 3 * * * user php7.2 /var/www/website/web/typo3/sysext/core/bin/typo3 scheduler:run`)
- backup setup
- monitoring client setup
- DNS setup
- HTTPS certificate

License
-------

    Copyright by Reelworx GmbH
    https://reelworx.at

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. (GPL.txt)
    If not, see <http://www.gnu.org/licenses/>.
    
    Contact: support@reelworx.at
